#include "Controller/MainController.h"
#include "Controller/CustomerCreationController.h"



int main()
{
	Bank* bank1 = new Bank();
	AccountManager* accountManager = new AccountManager(bank1);

	MainView* mainView = new MainView();
	CustomerCreationView* customerCreationView = new CustomerCreationView();
	CustomerListView* customerListView = new CustomerListView();
	BankOperationsMenu* bankOperationsMenu = new BankOperationsMenu();
	ChooseUserView* chooseUserView = new ChooseUserView();
	CreateAccountView* createAccountView = new CreateAccountView();
	DepositView* depositView = new DepositView();
	AccountBalanceView* accountBalanceView = new AccountBalanceView();


	MainController* mainController = new MainController();
	mainController->setMainView(mainView);


	CustomerCreationController* customerCreationController = new CustomerCreationController();
	customerCreationController->setCustomerCreationView(customerCreationView);
	customerCreationController->setAccountManager(accountManager);
	mainController->setCustomerCreationController(customerCreationController);

	CustomerListController* customerListController = new CustomerListController();
	customerListController->setCustomerListView(customerListView);
	customerListController->setAccountManager(accountManager);
	mainController->setCustomerListController(customerListController);

	BankOperationsController* bankOperationsController = new BankOperationsController();
	bankOperationsController->setBankOperationsMenu(bankOperationsMenu);
	mainController->setBankOperationsController(bankOperationsController);

	ChooseUserController* chooseUserController = new ChooseUserController();
	chooseUserController->setChooseUserView(chooseUserView);
	chooseUserController->setAccountManager(accountManager);
	bankOperationsController->setChooseUserController(chooseUserController);

	CreateAccountController* createAccountController = new CreateAccountController();
	createAccountController->setCreateAccountView(createAccountView);
	createAccountController->setChooseUserController(chooseUserController);
	createAccountController->setAccountManager(accountManager);
	bankOperationsController->setCreateAccountController(createAccountController);

	DepositController* depositController = new DepositController();
	depositController->setDepositView(depositView);
	depositController->setAccountManager(accountManager);
	depositController->setChooseUserController(chooseUserController);
	bankOperationsController->setDepositController(depositController);

	AccountBalanceController* accountBalanceController = new AccountBalanceController();
	accountBalanceController->setAccountBalanceView(accountBalanceView);
	accountBalanceController->setChooseUserController(chooseUserController);
	accountBalanceController->setAccountManager(accountManager);
	bankOperationsController->setAccountBalanceController(accountBalanceController);

	bankOperationsController->init();
	mainController->init();
	mainController->showMenus();

	return 0;
}