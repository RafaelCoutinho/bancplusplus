#pragma once
#include"../View/AccountBalanceView.h"
#include"../Model/AccountManager.h"
#include"ChooseUserController.h"

class AccountBalanceController : public Controller
{
	public:
		~AccountBalanceController();

		virtual void show();
		void getCustomerAccountIds();
		void getAccountBalance();

		void setAccountBalanceView(AccountBalanceView* accountBalanceView);
		void setAccountManager(AccountManager* accountManager);
		void setChooseUserController(ChooseUserController* chooseUserController);


	private:
		AccountBalanceView* accountBalanceView;
		ChooseUserController* chooseUserController;
		AccountManager* accountManager;
};
