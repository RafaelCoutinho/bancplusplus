#include "BankOperationsController.h"

BankOperationsController::~BankOperationsController()
{
	bankOperationsMenu = NULL;
	chooseUserController = NULL;
	createAccountController = NULL;
}

void BankOperationsController::init()
{
	createBankOperationsMap();
}

void BankOperationsController::show()
{
	if (bankOperationsMenu)
	{
		bankOperationsMenu->printBankOperationsMenu();
		performBankOperationChoice();
	}
	if (chooseUserController->getUserInput() != "B")
	{
		this->show();
	}
}


void BankOperationsController::performBankOperationChoice()
{
	unsigned int userChoise = bankOperationsMenu->getUserChoice();
	userChoise -= 1;

	if (userChoise < bankOperationsControllerList.size())
	{
		bankOperationsControllerList[userChoise]->show();
	}
}


void BankOperationsController::createBankOperationsMap()
{
	bankOperationsControllerList.push_back(chooseUserController);
	bankOperationsControllerList.push_back(createAccountController);
	bankOperationsControllerList.push_back(depositController);
	bankOperationsControllerList.push_back(accountBalanceController);
}


void BankOperationsController::setBankOperationsMenu(BankOperationsMenu* bankOperationsMenu)
{
	this->bankOperationsMenu = bankOperationsMenu;
}

void BankOperationsController::setChooseUserController(ChooseUserController* chooseUserController)
{
	this->chooseUserController = chooseUserController;
}


void BankOperationsController::setCreateAccountController(CreateAccountController* createAccountController)
{
	this->createAccountController = createAccountController;
}


void BankOperationsController::setDepositController(DepositController* depositController)
{
	this->depositController = depositController;
}


void BankOperationsController::setAccountBalanceController(AccountBalanceController* accountBalanceController)
{
	this->accountBalanceController = accountBalanceController;
}