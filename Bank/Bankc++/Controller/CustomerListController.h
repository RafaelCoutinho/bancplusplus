#pragma once
#include"Controller.h"
#include"../Model/AccountManager.h"
#include "../View/CustomerListView.h"

class CustomerListController : public Controller
{
   public:
	   CustomerListController();
	   ~CustomerListController();

	   virtual void show();
	   void customerListGetter();
	   void setCustomerListView(CustomerListView* customerListView);
	   void setAccountManager(AccountManager* accountManagerIn);

   private:
	   AccountManager* accountManager;
	   CustomerListView* customerListView;

};