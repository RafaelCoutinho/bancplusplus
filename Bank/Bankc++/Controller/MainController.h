#pragma once
#include "../View/MainView.h"
#include "Controller.h"
#include "CustomerCreationController.h"
#include "CustomerListController.h"
#include "BankOperationsController.h"

class MainController
{
	public:
		MainController();
		~MainController();

		void init();
		void controllerMap();
		void showMenus();

		void setMainView(MainView* mainview);
		void setCustomerCreationController(CustomerCreationController* mainViewController);
		void setCustomerListController(CustomerListController* customerListController);
		void setBankOperationsController(BankOperationsController* bankOperationsController);


	private:
		std::vector<Controller*> controllerList;
		MainView* mainView;
		CustomerCreationController* customerCreationController;
		CustomerListController* customerListController;
		BankOperationsController* bankOperationsController;

};