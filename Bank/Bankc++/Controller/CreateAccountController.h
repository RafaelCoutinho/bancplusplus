#pragma once
#include"Controller.h"
#include"../View/CreateAccountView.h"
#include"../Model/AccountManager.h"
#include"ChooseUserController.h"

class CreateAccountController : public Controller
{
	public:
		CreateAccountController();
		~CreateAccountController();

		virtual void show();
		int generateNewAccount(Customer* currentCustomer);

		void setCreateAccountView(CreateAccountView* createAccountView);
		void setAccountManager(AccountManager* accountManager);
		void setChooseUserController(ChooseUserController* chooseUserController);

	private:
		CreateAccountView* createAccountView;
		AccountManager* accountManager;
		ChooseUserController* chooseUserController;
};
