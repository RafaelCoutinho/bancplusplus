#include"ChooseUserController.h"

ChooseUserController::ChooseUserController()
{
	chooseUserView = NULL;
	accountManager = NULL;
	currentCustomer = NULL;
	userInput.clear();
}

ChooseUserController::~ChooseUserController()
{
	chooseUserView = NULL;
	accountManager = NULL;
	currentCustomer = NULL;
	userInput.clear();
}


void ChooseUserController::show()
{
	if (!chooseUserView)
	{
		return;
	}
	chooseUserView->printChooseUserView();
	userInput = chooseUserView->getUserName();
	if (userInput == "B")
	{
		return;
	}
	if (!isUserNameValide(userInput))
	{
		chooseUserView->invalidUserNameMessage();
		show();
	}
	else
	{
		chooseUserView->printWelcomeStatement();
	}
}


bool ChooseUserController::isUserNameValide(std::string currentName)
{
	bool isUserNameValid = false;
	if (!accountManager || !chooseUserView)
	{
		return isUserNameValid;
	}

	std::vector<Customer*> customerList = accountManager->getCustomerList();

	for (unsigned int customerIndex = 0; customerIndex < customerList.size(); customerIndex++)
	{
		Customer* customer = customerList.at(customerIndex);
		std::string customerName = customer->getCustomerName();
		if (currentName == customerName)
		{
			currentCustomer = customer;
			isUserNameValid = true;
			break;
		}
	}

	return isUserNameValid;
}


void ChooseUserController::setChooseUserView(ChooseUserView* chooseUserView)
{
	this->chooseUserView = chooseUserView;
}

void ChooseUserController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}


Customer* ChooseUserController::getCurrentCustomer()
{
	return currentCustomer;
}

std::string ChooseUserController::getUserInput()
{
	return userInput;
}