#pragma once
class Controller
{
	public:
		virtual void show() = 0;
};