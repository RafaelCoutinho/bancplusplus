#include "CustomerListController.h"

CustomerListController::CustomerListController()
{
	accountManager = NULL;
	customerListView = NULL;
}

CustomerListController::~CustomerListController()
{
	accountManager = NULL;
	customerListView = NULL;
}


void CustomerListController::customerListGetter()
{
	std::vector<Customer*> customerList = accountManager->getCustomerList();
	for (unsigned int customerIndex = 0; customerIndex < customerList.size(); customerIndex++)
	{
		std::string customerName = customerList[customerIndex]->getCustomerName();
		customerListView->printCustomerList(customerName);
	}
}

void CustomerListController::show()
{
	customerListGetter();
}


void CustomerListController::setCustomerListView(CustomerListView* customerListView)
{
	this->customerListView = customerListView;
}


void CustomerListController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}