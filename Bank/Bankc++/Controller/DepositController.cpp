#include"DepositController.h"


DepositController::DepositController()
{
	currentSelectedAccount = NULL;
}

DepositController::~DepositController()
{
	currentSelectedAccount = NULL;
}


void DepositController::show()
{
	accountChoice();
}


void DepositController::accountChoice()
{
	if (depositView)
	{
		depositView->selectAccountIDMessage();
		currentSelectedAccount = accountManager->selectedAccountFromCustomer(depositView->getSelectedAccountID(), chooseUserController->getCurrentCustomer());
	}
	if (!currentSelectedAccount)
	{
		depositView->invalidUserMessage();
		std::string userChoice = depositView->getUserChoice();
		if (userChoice == "B")
		{
			return;
		}
		accountChoice();
	}

	depositView->printDepositAmountView();
	performeDeposit(depositView->getAmountDeposited());
}



void DepositController::performeDeposit(int amount)
{
	Customer* currentCustomer = chooseUserController->getCurrentCustomer();
	if (!currentCustomer)
	{
		depositView->invalidUserMessage();
		return;
	}
	accountManager->deposit(currentCustomer, currentSelectedAccount, amount);
	depositView->printDepositedView(currentCustomer->getCustomerName(), amount);
}


void DepositController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}

void DepositController::setChooseUserController(ChooseUserController* chooseUserController)
{
	this->chooseUserController = chooseUserController;
}

void DepositController::setDepositView(DepositView* depositView)
{
	this->depositView = depositView;
}