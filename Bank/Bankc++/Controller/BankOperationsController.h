#pragma once
#include "Controller.h"
#include "../Model/BankOperationsMenu.h"
#include"ChooseUserController.h"
#include"CreateAccountController.h"
#include"DepositController.h"
#include"AccountBalanceController.h"

class BankOperationsController : public Controller
{
	public:
		~BankOperationsController();

		virtual void show();
		void init();

		void setBankOperationsMenu(BankOperationsMenu* bankOperationsMenu);
		void setChooseUserController(ChooseUserController* chooseUserController);
		void setCreateAccountController(CreateAccountController* createAccountController);
		void setDepositController(DepositController* depositController);
		void setAccountBalanceController(AccountBalanceController* accountBalanceController);



	private:
		BankOperationsMenu* bankOperationsMenu;
		ChooseUserController* chooseUserController;
		CreateAccountController* createAccountController;
		DepositController* depositController;
		AccountBalanceController* accountBalanceController;
		std::vector<Controller*> bankOperationsControllerList;

		void performBankOperationChoice();
		void createBankOperationsMap();

};
