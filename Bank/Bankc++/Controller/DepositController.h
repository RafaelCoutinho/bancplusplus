#pragma once
#include"Controller.h"
#include"../View/DepositView.h"
#include"../Model/AccountManager.h"
#include "ChooseUserController.h"

class DepositController : public Controller
{
	public:
		virtual void show();
		DepositController();
		~DepositController();
		void accountChoice();
		void performeDeposit(int amount);

		void setAccountManager(AccountManager* accountManager);
		void setDepositView(DepositView* depositView);
		void setChooseUserController(ChooseUserController* chooseUserController);

	private:
		AccountManager* accountManager;
		DepositView* depositView;
		ChooseUserController* chooseUserController;
		Account* currentSelectedAccount;
};
