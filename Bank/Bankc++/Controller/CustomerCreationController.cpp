#include "CustomerCreationController.h"

CustomerCreationController::CustomerCreationController()
{
	customerCreationView = NULL;
	accountManager = NULL;
}

CustomerCreationController::~CustomerCreationController()
{
	customerCreationView = NULL;
	accountManager = NULL;
}


void CustomerCreationController::show()
{
	Customer* newCustomer = createNewCustomer();
	addCustomerName(newCustomer);
	customerCreationView->welcomeStatement(newCustomer->getCustomerName());

}

Customer* CustomerCreationController::createNewCustomer()
{
	Customer* newCustomer = NULL;
	if (accountManager)
	{
		newCustomer = accountManager->createNewCustomer();
	}
	return newCustomer;
}


void CustomerCreationController::addCustomerName(Customer* customer)
{
	if (!customer)
	{
		return;
	}
	std::string customerName = customerCreationView->requestCustomerName();
	accountManager->customerNameAtribution(customerName, customer->getCustomerId());
}



void CustomerCreationController::setCustomerCreationView(CustomerCreationView* customerCreationView)
{
	if (!customerCreationView)
	{
		this->customerCreationView = customerCreationView;
	}
}


void CustomerCreationController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}