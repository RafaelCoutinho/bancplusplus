#include "MainController.h"

MainController::MainController()
{
	mainView = NULL;
	customerCreationController = NULL;
	customerListController = NULL;
	bankOperationsController = NULL;
}

MainController::~MainController()
{
	mainView = NULL;
	customerCreationController = NULL;
	customerCreationController = NULL;
	bankOperationsController = NULL;
}

void MainController::init()
{
	controllerMap();
}


void MainController::controllerMap()
{
	controllerList.push_back(customerCreationController);
	controllerList.push_back(customerListController);
	controllerList.push_back(bankOperationsController);
}


void MainController::showMenus()
{
	unsigned int userChoice = mainView->printMainView();
	userChoice -= 1;
	if (userChoice < controllerList.size())
	{
		controllerList[userChoice]->show();
	}
	showMenus();
}


void MainController::setMainView(MainView* mainView)
{
	this->mainView = mainView;
}


void MainController::setCustomerCreationController(CustomerCreationController* mainViewController)
{
	this->customerCreationController = mainViewController;
}


void MainController::setCustomerListController(CustomerListController* customerListController)
{
	this->customerListController = customerListController;
}

void MainController::setBankOperationsController(BankOperationsController* bankOperationsController)
{
	this->bankOperationsController = bankOperationsController;
}
