#include"CreateAccountController.h"

CreateAccountController::CreateAccountController()
{
	createAccountView = NULL;
	accountManager = NULL;
	chooseUserController = NULL;
}

CreateAccountController::~CreateAccountController()
{
	createAccountView = NULL;
	accountManager = NULL;
	chooseUserController = NULL;
}

void CreateAccountController::show()
{
	if (!createAccountView && !chooseUserController)
	{
		return;
	}
	Customer* currentCustomer = chooseUserController->getCurrentCustomer();
	if (!currentCustomer)
	{
		createAccountView->createAccountErrorMessage();
		return;
	}
	int accountID = generateNewAccount(currentCustomer);
	std::string currentCustomerName = currentCustomer->getCustomerName();
	createAccountView->printCreateAccountView(currentCustomerName, accountID);
}


int CreateAccountController::generateNewAccount(Customer* currentCustomer)
{
	int accountID = 0;
	if (accountManager)
	{
		Account* newAccount = accountManager->generateNewAccount(currentCustomer);

		std::map<Account*, Customer*> customerAccountList = accountManager->getCustomerAccountList();
		std::map<Account*, Customer*>::iterator pos = customerAccountList.find(newAccount);

		if (pos->second == currentCustomer)
		{
			accountID = pos->first->getAccountId();
		}
	}

	return accountID;
}


void CreateAccountController::setCreateAccountView(CreateAccountView* createAccountView)
{
	this->createAccountView = createAccountView;
}


void CreateAccountController::setChooseUserController(ChooseUserController* chooseUserController)
{
	this->chooseUserController = chooseUserController;
}


void CreateAccountController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}






