#pragma once
#include"Controller.h"
#include"../View/ChooseUserView.h"
#include"../Model/AccountManager.h"

class ChooseUserController : public Controller
{
	public:
		ChooseUserController();
		~ChooseUserController();

		virtual void show();
		bool isUserNameValide(std::string currentName);

		void setChooseUserView(ChooseUserView* chooseUserView);
		void setAccountManager(AccountManager* accountManager);
		Customer* getCurrentCustomer();
		std::string getUserInput();

	private:
		ChooseUserView* chooseUserView;
		AccountManager* accountManager;
		Customer* currentCustomer;
		std::string userInput;


};