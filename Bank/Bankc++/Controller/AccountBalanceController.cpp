#include"AccountBalanceController.h"

AccountBalanceController::~AccountBalanceController()
{
	accountManager = NULL;
	chooseUserController = NULL;
}



void AccountBalanceController::show()
{
	getCustomerAccountIds();
	accountBalanceView->accountChoice();
	getAccountBalance();
}


void AccountBalanceController::getCustomerAccountIds()
{
	Customer* currentCustomer = chooseUserController->getCurrentCustomer();
	if (!currentCustomer)
	{
		return;
	}
	std::map<Account*, Customer*> customerAccountList = accountManager->getCustomerAccountList();
	std::map<Account*, Customer*>::iterator customerAccountListIt = customerAccountList.begin();

	for (customerAccountList.begin(); customerAccountListIt != customerAccountList.end(); ++customerAccountListIt)
	{
		if (customerAccountListIt->second == currentCustomer)
		{
			int accountId = customerAccountListIt->first->getAccountId();
			accountBalanceView->addAccountID(accountId);
		}
	}
}


void AccountBalanceController::getAccountBalance()
{
	std::string currentCustomerName = chooseUserController->getCurrentCustomer()->getCustomerName();
	int accountId = accountBalanceView->getAccountIdChoice();
	int amount = accountManager->getAccountAmount(accountId);
	accountBalanceView->printAccountBalanceView(amount, accountId, currentCustomerName);
}


void AccountBalanceController::setAccountBalanceView(AccountBalanceView* accountBalanceView)
{
	this->accountBalanceView = accountBalanceView;
}


void AccountBalanceController::setAccountManager(AccountManager* accountManager)
{
	this->accountManager = accountManager;
}


void AccountBalanceController::setChooseUserController(ChooseUserController* chooseUserController)
{
	this->chooseUserController = chooseUserController;
}
