#pragma once
#include "Controller.h"
#include "../View/CustomerCreationView.h"
#include "../Model/AccountManager.h"

class CustomerCreationController : public Controller
{
   public:
	   CustomerCreationController();
	   ~CustomerCreationController();

	   virtual void show();
	   void setCustomerCreationView(CustomerCreationView* customerCreationView);
	   void setAccountManager(AccountManager* accountManager);
	   Customer* createNewCustomer();
	   void addCustomerName(Customer* customer);

	private:
		CustomerCreationView* customerCreationView;
		AccountManager *accountManager;

};