#include "Customer.h"

Customer::Customer(int customerId)
{
	this->customerId = customerId;
	this->customerName.clear();
}

Customer::~Customer()
{
	this->customerId = 0;
}

void Customer::setCustomerName(std::string customerNameIn)
{
	customerName = customerNameIn;
}

std::string Customer::getCustomerName()
{
	return customerName;
}

int Customer::getCustomerId()
{
	return customerId;
}