#pragma once
#include "Account.h"
#include <map>
#include<string>

class Customer
{
	public:
		Customer(int customerId);
		~Customer();

		int getCustomerId();
		void setCustomerName(std::string customerNameIn);
		std::string getCustomerName();

	private:
		int customerId;
		std::string customerName;
};