#pragma once
#include "Customer.h"
#include "Bank.h"
#include <map>
#include <vector>

class AccountManager
{
   public:
	   AccountManager(Bank* Bank);
	   ~AccountManager();

	   Customer* createNewCustomer();
	   Account* generateNewAccount(Customer* customer);
	   void deposit(Customer* customer, Account* account, int amount);
	   void widthraw(Customer* customer, Account* account, int amount);
	   int getAccountAmount(int accountID);
	   void customerNameAtribution(std::string customerNameIn, int cusmtomerID);
	   std::vector<Customer*> getCustomerList();
	   std::map<Account*,Customer*> getCustomerAccountList();
	   Account* selectedAccountFromCustomer(int accountID, Customer* currentCustomer);

   private:
	   Bank* bank;
	   int currentCustomerId;
	   int currentAccountId;
	   std::map<Account*, Customer*> customerAccountList;
	   std::vector<Customer*> customerList;
	   int generateCustomerId();
	   int generateAccountId();
};
