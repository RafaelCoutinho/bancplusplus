#pragma once
#include <map>
#include "Account.h"
#include "Customer.h"

class Bank
{
	public:
		Bank();
		~Bank();
		void addAccount(int accountId, Account* account);
		void addCustomer(int customerId, Customer* customer);
		void deposit(int accountid, int amount);
		void widthraw(int accountId, int amount);
		std::map<int, Account*> getAccountList();

	private:
		std::map<int, Customer*> customerList;
		std::map<int, Account*> accountList;
};
