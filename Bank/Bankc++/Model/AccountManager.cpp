#include "AccountManager.h"
#include<iostream>

AccountManager::AccountManager(Bank* bank)
{
	currentCustomerId = 0;
	currentAccountId = 0;
	this->bank = bank;
}


AccountManager::~AccountManager()
{
	currentCustomerId = 0;
	currentAccountId = 0;
	this->bank = NULL;
}


int AccountManager::generateCustomerId()
{
	currentCustomerId++;
	return currentCustomerId;
}


int AccountManager::generateAccountId()
{
	currentAccountId++;
	return currentAccountId;
}


Customer* AccountManager::createNewCustomer()
{
	int newCustomerId = generateCustomerId();
	Customer* customer = new Customer(newCustomerId);

	bank->addCustomer(newCustomerId, customer);
	customerList.push_back(customer);
	return customer;
}


Account* AccountManager::generateNewAccount(Customer* customer)
{
	int accountId = generateAccountId();
	Account* account = new Account(accountId);

	customerAccountList.insert(std::pair<Account*,Customer*>(account, customer));
	bank->addAccount(accountId, account);

	return account;
}


void AccountManager::deposit(Customer* customer, Account* account, int amount)
{
	if (customerAccountList.find(account) != customerAccountList.end())
	{
		if (customerAccountList[account]->getCustomerId() == customer->getCustomerId())
		{
			bank->deposit(account->getAccountId(), amount);
		}
	}
}


void AccountManager::widthraw(Customer* customer, Account* account, int amount)
{
	if (customerAccountList.find(account) != customerAccountList.end())
	{
		if (customerAccountList[account]->getCustomerId() == customer->getCustomerId())
		{
			bank->widthraw(account->getAccountId(), amount);
		}
	}
}


void AccountManager::customerNameAtribution(std::string customerNameIn, int cusmtomerID)
{
	for (unsigned customerIndex = 0; customerIndex < customerList.size(); customerIndex++)
	{
		if (customerList.at(customerIndex)->getCustomerId() == cusmtomerID)
		{
			
			customerList.at(customerIndex)->setCustomerName(customerNameIn);
			break;
	    }
	}
}


Account* AccountManager::selectedAccountFromCustomer(int accountID, Customer* currentCustomer)
{
	Account* selectedAccountFromCustomer = NULL;

	if (accountID <= 0 || accountID > currentAccountId)
	{
		selectedAccountFromCustomer = NULL;
		return selectedAccountFromCustomer;
	}
	
	std::map<int,Account*> accountList = bank->getAccountList();
	std::map<int, Account*>::iterator accountListIterator = accountList.find(accountID);
	std::map<Account*, Customer*>::iterator accountCustomerIt;

	if (accountListIterator != accountList.end())
	{
		accountCustomerIt = customerAccountList.find(accountListIterator->second);
	}
	if (accountCustomerIt != customerAccountList.end())
	{
		selectedAccountFromCustomer = (accountCustomerIt->second == currentCustomer) ? accountCustomerIt->first : NULL;
	}

	return selectedAccountFromCustomer;
}


int AccountManager::getAccountAmount(int accountID)
{
	int amount = 0;
	std::map<int, Account*> accountList = bank->getAccountList();
	if (accountList.find(accountID) != accountList.end())
	{
		amount = accountList[accountID]->getAmount();
	}
	return amount;
}

std::vector<Customer*> AccountManager::getCustomerList()
{
	return customerList;
}


std::map<Account*,Customer*> AccountManager::getCustomerAccountList()
{
	return customerAccountList;
}