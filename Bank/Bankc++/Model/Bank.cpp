#include "Bank.h"

Bank::Bank()
{
}

Bank::~Bank()
{
}

void Bank::addAccount(int accountId, Account* account)
{
	accountList.insert(std::pair<int, Account*>(accountId, account));
}


void Bank::addCustomer(int customerId, Customer* customer)
{
	customerList.insert(std::pair<int, Customer*>(customerId, customer));
}


void Bank::deposit(int accountId, int amount)
{
	if (accountList.find(accountId) != accountList.end())
	{
		accountList[accountId]->setDepositedAmount(amount);
	}
}

void Bank::widthraw(int accountId, int amount)
{
	if (accountList.find(accountId) != accountList.end())
	{
		Account* account = accountList[accountId];
		int currentAccountAmount = account->getAmount();
		if (currentAccountAmount > amount)
		{
			int newAmount = currentAccountAmount - amount;
			account->setDepositedAmount(newAmount);
	    }
	}
}


std::map<int, Account*> Bank::getAccountList()
{
	return accountList;
}