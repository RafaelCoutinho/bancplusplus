#include "BankOperationsMenu.h"

BankOperationsMenu::BankOperationsMenu()
{
	userChoice = 0;
}

BankOperationsMenu::~BankOperationsMenu()
{
	userChoice = 0;
}

void BankOperationsMenu::printBankOperationsMenu()
{
	std::cout << "\n1 - Choose user." << std::endl;
	std::cout << "2 - Create an account." << std::endl;
	std::cout << "3 - Deposite." << std::endl;
	std::cout << "4 - Widthraw." << std::endl;
	std::cout << "5 - Check account balance" << std::endl;
	userChoiceFromMenu();
}

void BankOperationsMenu::userChoiceFromMenu()
{
	userChoice = 0;
	std::cout << "\nPick you option: ";
	std::cin >> userChoice;
	std::cout << "\n";
}

int BankOperationsMenu::getUserChoice()
{
	return userChoice;
}