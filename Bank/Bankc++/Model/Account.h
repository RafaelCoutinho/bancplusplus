#pragma once

class Account
{
   public:
	   Account(int accountId);
	   ~Account();

	   void setDepositedAmount(int amount);
	   void setWidthrawnAmout(int amount);
	   int getAmount();
	   int getAccountId();

   private:
	   int accountId;
	   unsigned int amount;
};