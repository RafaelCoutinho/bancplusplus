#pragma once
#include <iostream>

class BankOperationsMenu
{
	public:
		BankOperationsMenu();
		~BankOperationsMenu();

		void printBankOperationsMenu();
		void userChoiceFromMenu();
		int getUserChoice();

	private:
		int userChoice;
};