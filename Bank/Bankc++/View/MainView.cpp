#include "MainView.h"

MainView::MainView()
{
	userChoice = 0;
}

MainView::~MainView()
{
}


int MainView::printMainView()
{
	std::cout << "1. Insert new user.\n";
	std::cout << "2. Customer list.\n";
	std::cout << "3. Bank operations. \n";

	return userInput();
}

int MainView::userInput()
{
	int userInput;
	std::cout << "\nChoose option: ";
	std::cin >> userInput;

	userChoice = userInput;

	return userChoice;
}


int MainView::getUserChoice()
{
	return userChoice;
}