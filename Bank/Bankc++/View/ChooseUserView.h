#pragma once
#include<iostream>
#include<string>

class ChooseUserView
{
	public:
		ChooseUserView();
		~ChooseUserView();

		void printChooseUserView();
		void invalidUserNameMessage();
		void printWelcomeStatement();

		std::string getUserName();

	private:
		std::string userName;
};
