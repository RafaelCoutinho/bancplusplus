#include"DepositView.h"

DepositView::DepositView()
{
	amountDeposited = 0;
	userchoice.clear();
}


DepositView::~DepositView()
{
	amountDeposited = 0;
}


void DepositView::selectAccountIDMessage()
{
	std::cout << "Insert you account ID:";
    std::cin >> selectedAccountId;
}


void DepositView::printDepositAmountView()
{
	std::cout << "Please, insert the amount of money that you want to deposit: ";
	std::cin >> amountDeposited;
}


void DepositView::printDepositedView(std::string customerName, int deposit)
{
	std::cout << "\n" << customerName << ", you have deposited " << deposit << " euros." << std::endl;
}


void DepositView::invalidUserMessage()
{
	std::cout << "\nPlease, choose a registered user (by pressing B) or introduce a valid account for the current user.\n";
	std::cin >> userchoice;
}


int DepositView::getAmountDeposited()
{
	return amountDeposited;
}

int DepositView::getSelectedAccountID()
{
	return selectedAccountId;
}

std::string DepositView::getUserChoice()
{
	return userchoice;
}