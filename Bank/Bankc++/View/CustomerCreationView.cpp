#include "CustomerCreationView.h"

CustomerCreationView::CustomerCreationView()
{
}

CustomerCreationView::~CustomerCreationView()
{
}


std::string CustomerCreationView::requestCustomerName()
{
	std::string customerName;
	customerName.clear();

	std::cout << "\nEnter your name, please: ";
	std::cin >> customerName;

	return customerName;
}


void CustomerCreationView::welcomeStatement(std::string customerName)
{
	if (!customerName.empty())
	{
		std::cout << "\nWelcome, " << customerName << "\n\n";
		return;
	}
}