#pragma once
#include<iostream>

class DepositView
{
	public:
		DepositView();
		~DepositView();

		void printDepositAmountView();
		int getAmountDeposited();
		void invalidUserMessage();
		void selectAccountIDMessage();
		int getSelectedAccountID();
		void printDepositedView(std::string customerName, int deposit);
		std::string getUserChoice();

	private:
		int amountDeposited;
		int selectedAccountId;
		std::string userchoice;
};
