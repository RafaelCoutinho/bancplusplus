#include"ChooseUserView.h"

ChooseUserView::ChooseUserView()
{
	userName.clear();
}

ChooseUserView::~ChooseUserView()
{
	userName.clear();
}


void ChooseUserView::printChooseUserView()
{
	std::cout << "\nInsert your user name: " << std::endl;
	std::cin >> userName;
}


void ChooseUserView::invalidUserNameMessage()
{
	std::cout << "There is no such user name.\n\n" << "Insert a valid user name or press 'B' to return to the Main Menu.\n\n";
}


void ChooseUserView::printWelcomeStatement()
{
	std::cout << "Welcome, " << userName << std::endl;
}



std::string ChooseUserView::getUserName()
{
	return userName;
}