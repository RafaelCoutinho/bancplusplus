#pragma once
#include<iostream>

class CustomerListView
{
   public:
	   CustomerListView();
	   ~CustomerListView();

	   void printCustomerList(std::string customerName);
};