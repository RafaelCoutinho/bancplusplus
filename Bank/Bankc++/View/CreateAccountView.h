#pragma once
#include<iostream>
#include<string>

class CreateAccountView
{
	public:
		CreateAccountView();
		~CreateAccountView();

		void printCreateAccountView(std::string customerName, int accountID);
		void createAccountErrorMessage();
};
