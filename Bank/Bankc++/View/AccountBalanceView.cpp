#include"AccountBalanceView.h"

AccountBalanceView::AccountBalanceView()
{
	userChoice = 0;
}

AccountBalanceView::~AccountBalanceView()
{
	userChoice = 0;
}


void AccountBalanceView::printAccountBalanceView(int amount, int accountID, std::string customerName)
{
	std::cout << std::endl << customerName << ", your account number " << accountID << " has " << amount << " euros." << std::endl;
	accountIDList.clear();
}

void AccountBalanceView::accountChoice()
{
	std::cout << std::endl << "Chose an account:" << std::endl;
	for (unsigned int i = 0; i < accountIDList.size(); i++)
	{
		std::cout << std::endl << "Account: " << accountIDList[i] << std::endl;
	}
	std::cin >> userChoice;
}


void AccountBalanceView::addAccountID(int accountID)
{
	accountIDList.push_back(accountID);
}


int AccountBalanceView::getAccountIdChoice()
{
	userChoice;
	return accountIDList[userChoice - 1];
}