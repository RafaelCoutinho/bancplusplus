#include"CreateAccountView.h"

CreateAccountView::CreateAccountView()
{
}

CreateAccountView::~CreateAccountView()
{
}


void CreateAccountView::printCreateAccountView(std::string customerName, int accountID)
{
	std::cout << "Dear " << customerName << ", your account has been created with the ID " << accountID << std::endl;
}


void CreateAccountView::createAccountErrorMessage()
{
	std::cout << "Please, create or pick an user first.";
}