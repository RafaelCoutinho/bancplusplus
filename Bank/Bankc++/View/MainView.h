#pragma once
#include<iostream>
#include<string>
#include"../Utils.h"

class MainView
{
	public:
		MainView();
		~MainView();
		int printMainView();
		int getUserChoice();

private:
	int userChoice;

	int userInput();
};