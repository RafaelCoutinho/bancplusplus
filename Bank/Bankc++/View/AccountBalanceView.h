#pragma once
#include<iostream>
#include<vector>

class AccountBalanceView
{
   #define NO_ACCOUNTS_CREATED 0

	public:
		AccountBalanceView();
		~AccountBalanceView();

		void printAccountBalanceView(int amount, int accountID, std::string customerName);
		void accountChoice();
		void addAccountID(int accountID);
		int getAccountIdChoice();

	private:
		int userChoice;
		std::vector<int> accountIDList;
};
