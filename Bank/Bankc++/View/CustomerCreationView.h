#pragma once
#include<string>
#include<iostream>

class CustomerCreationView
{
	public:
		CustomerCreationView();
		~CustomerCreationView();

		std::string requestCustomerName();
		void welcomeStatement(std::string customerName);
};